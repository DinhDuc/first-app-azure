
<html>
<head>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
  <title>SSO WITH FACEBOOK AUTHENZITION</title>
</head>
<body>
<?php
    echo "<quote>";
    echo 'HTTP_CLIENT_IP: ' . $_SERVER["HTTP_CLIENT_IP"] . "<br>";
    echo 'HTTP_X_FORWARDED_FOR: ' . $_SERVER["HTTP_X_FORWARDED_FOR"] . "<br>";
    echo 'HTTP_X_FORWARDED: ' . $_SERVER["HTTP_X_FORWARDED"] . "<br>";
    echo 'HTTP_X_CLUSTER_CLIENT_IP: ' . $_SERVER["HTTP_X_CLUSTER_CLIENT_IP"] . "<br>";
    echo 'HTTP_FORWARDED_FOR: ' . $_SERVER["HTTP_FORWARDED_FOR"] . "<br>";
    echo 'HTTP_FORWARDED: ' . $_SERVER["HTTP_FORWARDED"] . "<br>";
    echo 'REMOTE_ADDR: ' . $_SERVER["REMOTE_ADDR"] . "<br>";             
    echo "</quote>";
    define("_APP_ID","597958960997390");
    define("_APP_SECRET",""); 
    define("_VERSION","v5.0");
    $conn_src = "https://connect.facebook.net/ja_JP/sdk.js";
?>

<script>

  function statusChangeCallback(response) {  // Called with the results from FB.getLoginStatus().
    console.log('statusChangeCallback');
    console.log(response);                   // The current login status of the person.
    if (response.status === 'connected') {   // Logged into your webpage and Facebook.
      testAPI();
      $('#loginBtn').hide();  
    } else {                                 // Not logged into your webpage or we are unable to tell.
      $('#status').text('Please log ' + 'into this webpage.');
      $('#loginBtn').show();
    }
  }


  function checkLoginState() {               // Called when a person is finished with the Login Button.
    FB.getLoginStatus(function(response) {   // See the onlogin handler
      statusChangeCallback(response);
    });
  }


  window.fbAsyncInit = function() {
    FB.init({
      appId      : '<?=_APP_ID?>',
      cookie     : true,                     // Enable cookies to allow the server to access the session.
      xfbml      : true,                     // Parse social plugins on this webpage.
      version    : '<?=_VERSION?>'           // Use this Graph API version for this call.
    });


    FB.getLoginStatus(function(response) {   // Called after the JS SDK has been initialized.
      statusChangeCallback(response);        // Returns the login status.
    });
  };

  
  (function(d, s, id) {                      // Load the SDK asynchronously
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "<?=$conn_src?>";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

 
  function testAPI() {                      // Testing Graph API after login.  See statusChangeCallback() for when this call is made.
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
      console.log('Successful login for: ' + response.name);
      $('#status').text('Thanks for logging in, ' + response.name + '!');
    });
  }

</script>
//  The JS SDK Login Button 
<div id='loginBtn'>
<fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
</fb:login-button>
</div>
<fb:logout-button>
    
</fb:logout-button>
<div id="status">
</div>
</body>
</html>